//En esta sección se están incluyendo las librerías necesarias para la ejecución del programa.
#include <iostream>//Libreria de entrada y salida de datos en consola
#include <iomanip>//Libreria que se encagar de ajustar el numero de espacio entre los numeros y datos
#include <cmath>//Libreria de funciones matematica
#include <conio.h>
#include <stdio.h>//Permite realizar acciones en consola
#include <stdlib.h>//Libreria que limpia la pantalla de consola
//En esta sección se declaran las variables que se utilizarán en el programa.
using namespace std;
double X0, X1, X2, e, P;
int iter = 1, n;
//En esta sección se define la función f(x) que será utilizada en el método de la secante.
double f(double x) {
	//Aqui usted debe ingresar la funcion que desea calcular en metodos numericos 
    double a = 2*pow(x, 4) + 3*x -1;
    return a;
}
//Esta función el usuario va a confirmar si desdea continuar con el programa.
bool deseaContinuar() {
    char respuesta;
    cout << "\nDesea continuar? (s/n): ";
    cin >> respuesta;

    if (respuesta == 's' || respuesta == 'S') {
        return true;
    } else {
        return false;
    }
}
//En esta sección del código, se muestra un mensaje de bienvenida y se solicitan algunos valores
int main() {
    cout << "CALCULADORA DEL METODO DE LA SECANTE" << endl;
    cout << "Funcion a calcular es:  2*pow(x, 4) + 3*x -1"  << endl;
    do {
        cout << "Ingrese el valor inicial x0: ";
        cin >> X1;
        cout << "Ingrese el valor inicial x1: ";
        cin >> X2;
        cout << "Digitos despues del punto decimal: ";
        cin >> P;
        cout << "Ingrese la tolerancia deseada: ";
        cin >> e;
        cout.setf(ios::fixed);
        cout.precision(P);
        cout << "\nResultado de la secuencia:" << endl;
        cout << setw(2) << "Iter" << setw(8) << "X0" << setw(11) << "X1" << setw(12) << "X2" << setw(14) << "F(X0)" << setw(12) << "F(X1)" << setw(13) << "F(X2)" << setw(9) << "e" << endl;
        cout << "----------------------------------------------------------------------------------------------------\n";
        //Aquí comienza el bucle do-while interno, que ejecuta el método de la secante hasta que se alcance la tolerancia deseada (e).
        do {
            X0 = X1;
            X1 = X2;
            X2 = X1 - (f(X1)*(X0 - X1)) / (f(X0) - f(X1));
            cout << setw(2) << iter << setw(12) << X0 << setw(12) << X1 << setw(12) << X2 << setw(12) << f(X0) << setw(12) << f(X1) << setw(12) << f(X2) << setw(12) << fabs(((X2 - X1)/X2)*100) << endl;
            if (f(X2) == 0) {
                cout << "\nLa ecuacion es:" << X2;
                return 0;
            }
            n = iter;
            iter++;
        } while (abs(((X2 - X1)/X2)*100) > e);
        //Se muestra el número total de iteraciones realizadas (n) y el resultado final obtenido (X2).
        cout << "----------------------------------------------------------------------------------------------------\n";
        cout << "\nTotal de iteraciones: " << n << "\nEl resultado de raiz es: " << X1 << "\nEl resultado de la funcion de la raiz es:"<< f(X1);
        iter = 1;    
    } while (deseaContinuar());
    //Al finalizar el programa se mostrara un mensaje final
	cout << "Creado por:\nByron Alexander Santacruz Saltos,\nJoselyn Johanna Torres Toala,\nStefania Monserrate Mantuano Espinal" << endl;
    cout << "Gracias por usar nuestra app en c++..." << endl;
    getch();
    return 0;
}
